package com.besterinlove.databind;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.besterinlove.databind.databinding.FragmentDatabindBinding;

public class DataBindFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // FragmentDatabindBinding 為綁定的layout 名字+binding
        FragmentDatabindBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_databind, container, false);
        binding.tvBind.setText("Binding");
        return binding.getRoot();
    }
}
